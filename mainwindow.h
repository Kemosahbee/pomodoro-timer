#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QTimer>
#include <QMainWindow>
#include <QSettings>
#include <QDebug>
#include <QIcon>
#include <QCloseEvent>
#include <QStandardItem>
#include <QTime>
#include <QSqlTableModel>
#include <QSqlQuery>
#include <QMenu>
#include <QSqlRecord>
#include <QMenu>
#include <QPrinter>
#include <QPrintDialog>
#include <QPainter>
#include <QFont>
#include <QSpinBox>

#include "addtask.h"
#include "database.h"
#include "multisort.h"
#include "history.h"
#include "myprogressbar.h"

enum State {STOPPED=0, WORK, REST, INIT};

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

private slots:
    void on_addTaskButton_clicked();

    void slotCustomMenuRequested(QPoint pos);

    void on_deleteButton_clicked();

    void markAsDone();

    void setCurrentTask();

    void on_showHistoryButton_clicked();

    void on_startButton_clicked();

    void on_stopButton_clicked();

    void on_pomodoroButton_clicked();

    void on_restButton_clicked();

    void printTable();

    void exportTable();

    void on_applySettingsPushButton_clicked();

    void on_resetPushButton_clicked();

protected:
    void closeEvent(QCloseEvent *event) override;

private:
    void readSettings();

    void writeSettings();

    void createActions();

    void createMenus();

    void openDatabase();

    void createDatabaseTable();

    QString getAppFontProperties();

    QString getTableFontProperties();

    void setUserFontProperties(QString appFont, QString tableFont);


    QAction *newTaskAct;
    QAction *showHistoryAct;
    QAction *settingsAct;
    QAction *printTableAct;
    QAction *exportToPdfAct;
    QAction *exitAct;

    Ui::MainWindow *ui;
    QTimer timer;
    QTimer timer_2;
    AddTask *addTaskForm;
    QSqlTableModel *model;
    MultySortProxy *proxyModel;
    History *historyForm;
    QMenu *fileMenu;

    int currentTask=-1;

    int userWorkTime=30;
    int userRestTime=10;

    State state{INIT};
    State prevState{INIT};
    bool isStopButtonClicked;
};
#endif // MAINWINDOW_H
