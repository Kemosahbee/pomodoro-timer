#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    openDatabase();
    createDatabaseTable();

    addTaskForm = new AddTask;
    historyForm = new History;

    readSettings(); // Считываем настройки шрифтов

    setUserFontProperties(this->getAppFontProperties(), this->getTableFontProperties());
    historyForm->setUserFontProperties(this->getAppFontProperties(), this->getTableFontProperties());
    addTaskForm->setUserFontProperties(this->getAppFontProperties());

    createActions();
    createMenus();
//    this->setWindowState(Qt::WindowMaximized);

    model= new QSqlTableModel(this);
    model->setTable("TASKSLIST");
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->select();

    //proxyModel для сортировки по двум колонкам: Priority и Date
    proxyModel = new MultySortProxy();
    proxyModel->setSourceModel(model);

//    proxyModel->addSortPriority(3);
//    proxyModel->addSortPriority(0);
    proxyModel->sort(0);
    proxyModel->sort(3);
    //

    ui->tableView->setModel(proxyModel);
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows); // Выделять строку целиком
    ui->tableView->setContextMenuPolicy(Qt::CustomContextMenu); // Разрешить контекстное меню
    ui->tableView->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents); // Колонку Pomodooros растянуть по содержимому
    ui->tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch); // Колонку Task растянуть на все оставшееся место

    // Если строка не влазит в колонку, перенести на новую строку
    ui->tableView->setWordWrap(true); // Разрешить перенос слов
    ui->tableView->setTextElideMode(Qt::ElideMiddle);
    ui->tableView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    //

    ui->tableView->hideColumn(0); // Скрыть колонку Дата
    ui->tableView->hideColumn(3); // Скрыть колонку приоритет
    connect(ui->tableView, &QTableView::customContextMenuRequested,this,&MainWindow::slotCustomMenuRequested);

    const int oneMinute = 60000;
    timer.setInterval(oneMinute);
    timer_2.setInterval(oneMinute);

    // Таймер РАБОТА
    connect(&timer, &QTimer::timeout, ui->progressBar,
            [&]{
                if(ui->progressBar->value()>0){
                    ui->progressBar->setValue(ui->progressBar->value()-1);
                }else{
                    QApplication::beep();
                    timer.stop();
                    ui->progressBar->hide();
                    ui->progressBar_2->show();
                    timer_2.start();
                    ui->progressBar->setValue(userWorkTime);
                    prevState=state;
                    state=REST;
                    ui->label_4->setText("REST");

                    if(currentTask>=0){
                        QModelIndex index = proxyModel->index(currentTask, 2, QModelIndex());
                        QVariant pomodor = proxyModel->data(index);
                        proxyModel->setData(index, pomodor.toInt()+1);
                        model->submitAll();
                    }
                }
    });

    // Таймер ОТДЫХ
    connect(&timer_2, &QTimer::timeout, ui->progressBar_2,
            [&]{
                if(ui->progressBar_2->value()>0){
                    ui->progressBar_2->setValue(ui->progressBar_2->value()-1);
                }else{
                    // Если пользователю не хватило времени на вкладке Settings
                    //  во время перерыва, тогда остановит выполненение рабочего цикла
                    if(ui->tabWidget->currentIndex()==1)
                    {
                        on_stopButton_clicked();
                        return;
                    }

                    // Если пользователю не хватило времени в окне History
                    //  во время перерыва, тогда остановит выполненение рабочего цикла
                    if(historyForm->isVisible())
                    {
                        on_stopButton_clicked();
                        return;
                    }

                    QApplication::beep();
                    timer_2.stop();
                    ui->progressBar_2->hide();
                    ui->progressBar->show();
                    timer.start();
                    ui->progressBar_2->setValue(userRestTime);
                    prevState=state;
                    state=WORK;
                    ui->label_4->setText("WORK");
                }
    });

    ui->progressBar_2->hide();

    connect(addTaskForm, &AddTask::saveTask, addTaskForm,
            [&]{
                addTaskForm->insertIntoTable(addTaskForm->getText());
                model->select();
                ui->centralwidget->setEnabled(true);});

    connect(addTaskForm, &AddTask::cancelTask, ui->centralwidget, [&]{ui->centralwidget->setEnabled(true);});
    connect(addTaskForm, &AddTask::windowClosed, ui->centralwidget, [&]{ui->centralwidget->setEnabled(true);});

    connect(historyForm, &History::windowClosed, this,
            [&]{
                ui->centralwidget->setEnabled(true);
                if((prevState==WORK || prevState==REST) && isStopButtonClicked)
                {
                    on_startButton_clicked();
                }
     });

    // Если в разделе Settings не приняли изменения, то вернуть их
    connect(ui->tabWidget, &QTabWidget::currentChanged, this, [&]{readSettings();});

    // Остановить таймер, если пользователь вошел в раздел Settings во время работы
    connect(ui->tabWidget, &QTabWidget::currentChanged, this,
            [&]{
                if(timer.isActive()){
                    on_stopButton_clicked();
                }else{
                    isStopButtonClicked=false;
                }
    });

    // Настройка пользователем времени одного помидора
    connect(ui->progressBar, &QProgressBar::valueChanged, this,
            [&]{
                if(!timer.isActive()){
                    userWorkTime=ui->progressBar->value();
                }
    });

    // Настройка пользователем времени отдыха
    connect(ui->progressBar_2, &QProgressBar::valueChanged, this,
            [&]{
                if(!timer_2.isActive()){
                    userRestTime=ui->progressBar_2->value();
                }
    });

    QIcon plusIcon(":/icons/images/plus.png");
    ui->addTaskButton->setIcon(plusIcon);
    QIcon deleteIcon(":/icons/images/delete.png");
    ui->deleteButton->setIcon(deleteIcon);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
//    writeSettings();
    event->accept();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::writeSettings()
{
    QSettings settings("Ke-mo Sah-bee", "PomodoroApp");

    settings.beginGroup("App");
    settings.setValue("appFont", ui->appFontComboBox->currentFont());
    settings.setValue("appFontSize", ui->appTextSizeSpinBox->value());
    settings.setValue("appBold", ui->boldAppCheckBox->isChecked());
    settings.setValue("appItalic", ui->italicAppCheckBox->isChecked());
    settings.endGroup();

    settings.beginGroup("Table");
    settings.setValue("tableFont", ui->tableFontComboBox->currentFont());
    settings.setValue("tableFontSize", ui->tableTextSizeSpinBox->value());
    settings.setValue("tableBold", ui->boldTableCheckBox->isChecked());
    settings.setValue("tableItalic", ui->italicTableCheckBox->isChecked());
    settings.endGroup();

    ui->progressBar->style();
}

void MainWindow::readSettings()
{
    QSettings settings("Ke-mo Sah-bee", "PomodoroApp");

    settings.beginGroup("App");
    ui->appFontComboBox->setCurrentFont(QFont(settings.value("appFont").toString()));
    ui->appTextSizeSpinBox->setValue(settings.value("appFontSize").toInt());
    ui->boldAppCheckBox->setChecked(settings.value("appBold").toBool());
    ui->italicAppCheckBox->setChecked(settings.value("appItalic").toBool());
    settings.endGroup();

    settings.beginGroup("Table");
    ui->tableFontComboBox->setCurrentFont(QFont(settings.value("tableFont").toString()));
    ui->tableTextSizeSpinBox->setValue(settings.value("tableFontSize").toInt());
    ui->boldTableCheckBox->setChecked(settings.value("tableBold").toBool());
    ui->italicTableCheckBox->setChecked(settings.value("tableItalic").toBool());
    settings.endGroup();
}

void MainWindow::on_addTaskButton_clicked()
{
    addTaskForm->show();
    ui->centralwidget->setDisabled(true);
    if(timer.isActive() || timer_2.isActive()){
        on_stopButton_clicked();
    }else{
        isStopButtonClicked=false;
    }
}

void MainWindow::on_deleteButton_clicked()
{
    int r = ui->tableView->selectionModel()->currentIndex().row();
    if(r==currentTask)
    {
        currentTask=-1;
        on_pomodoroButton_clicked();
        ui->progressBar->setFormat("%v/%m");
        ui->progressBar_2->setFormat("%v/%m");
    }
    ui->tableView->model()->removeRow(r);
    model->submitAll();
}

// Контекстое меню
void MainWindow::slotCustomMenuRequested(QPoint pos)
{

    QMenu *menu = new QMenu(this);
    QAction *done = new QAction("Mark as done", this);
    QAction *current = new QAction("Set current task", this);
    menu->addAction(done);
    menu->addAction(current);
    connect(done, &QAction::triggered, this, &MainWindow::markAsDone);
    connect(current, &QAction::triggered, this, &MainWindow::setCurrentTask);
    menu->popup(ui->tableView->mapToGlobal(pos));
}


// Функция вызываемая с помощью контекстного меню
// Вставляем удаляемую строку в таблицу HISTORY в базе данных,
// поле чего удаляем строку
void MainWindow::markAsDone()
{
    if(!ui->tableView->selectionModel()->selectedRows().isEmpty())
    {
        int r = ui->tableView->selectionModel()->currentIndex().row();
        if(r==currentTask)
        {
            currentTask=-1;
            on_pomodoroButton_clicked();
            ui->progressBar->setFormat("%v/%m");
            ui->progressBar_2->setFormat("%v/%m");
        }
        QVariantList data;
        data.append(ui->tableView->model()->index(r,0).data().toString());
        data.append(ui->tableView->model()->index(r,1).data().toString());
        data.append(ui->tableView->model()->index(r,2).data().toInt());
        data.append(ui->tableView->model()->index(r,3).data().toInt());

        historyForm->insertIntoTable(data);
        historyForm->refresh();

        ui->tableView->model()->removeRow(r);
        model->submitAll();
    }
    return;
}

// Функция вызываемая с помощью контекстного меню
// Выбор текущей задачи для выполнения
void MainWindow::setCurrentTask()
{
    int r = ui->tableView->selectionModel()->currentIndex().row();
    if(r==currentTask) return;

    if(!ui->tableView->selectionModel()->selectedRows().isEmpty())
    {
        on_pomodoroButton_clicked();
        currentTask=r;
        QString task = ui->tableView->model()->index(r,1).data().toString();
        ui->progressBar->setFormat("Current task: " + task + "      %v/%m");
        ui->progressBar_2->setFormat("Current task: " + task + "      %v/%m");
    }
    return;
}

void MainWindow::on_showHistoryButton_clicked()
{
    historyForm->show();
    ui->centralwidget->setDisabled(true);
    if(timer.isActive()){
        on_stopButton_clicked();
    }else{
        isStopButtonClicked=false;
    }
}

void MainWindow::on_startButton_clicked()
{
    if(state==WORK || state==REST) return;

    ui->progressBar->setDisabled(1);
    ui->progressBar_2->setDisabled(1);

    if(state==STOPPED){
        if(prevState==INIT || prevState==WORK){
            ui->label_4->setText("WORK");
            timer.start();
            prevState=state;
            state=WORK;
        }else{
            ui->label_4->setText("REST");
            timer_2.start();
            prevState=state;
            state=REST;
        }
    }else if(state==INIT){
        prevState=state;
        state=WORK;
        ui->label_4->setText("WORK");
        timer.start();
        ui->label_3->setText("");
    }
}

void MainWindow::on_stopButton_clicked()
{
    if(state==STOPPED || state==INIT) return;

    if(state==WORK)
    {
        timer.stop();
    }else{
        timer_2.stop();
    }
    prevState=state;
    state=STOPPED;
    ui->label_4->setText("PAUSE");
    isStopButtonClicked=true;
}

void MainWindow::on_pomodoroButton_clicked()
{
    ui->progressBar->setValue(userWorkTime);
    ui->progressBar->show();
    ui->progressBar->setEnabled(1);

    ui->progressBar_2->setValue(userRestTime);
    ui->progressBar_2->hide();
    ui->progressBar_2->setEnabled(1);

    ui->label_4->clear();
    state=INIT;
    prevState=INIT;
    timer.stop();
    timer_2.stop();
}

void MainWindow::on_restButton_clicked()
{
    ui->progressBar->setValue(userWorkTime);
    ui->progressBar->hide();
    ui->progressBar->setEnabled(1);

    ui->progressBar_2->setValue(userRestTime);
    ui->progressBar_2->show();
    ui->progressBar_2->setEnabled(1);

    ui->label_4->clear();
    prevState=REST;
    state=STOPPED;
    timer.stop();
    timer_2.stop();
}

void MainWindow::createActions()
{
    newTaskAct = new QAction(tr("&New Task"), this);
    newTaskAct->setShortcuts(QKeySequence::New);
    newTaskAct->setStatusTip(tr("Create a new task"));
    connect(newTaskAct, &QAction::triggered, this, &MainWindow::on_addTaskButton_clicked);

    showHistoryAct = new QAction(tr("&Show History"), this);
    showHistoryAct->setStatusTip(tr("Show history"));
    connect(showHistoryAct, &QAction::triggered, this, &MainWindow::on_showHistoryButton_clicked);

    printTableAct = new QAction(tr("&Print Table"), this);
    printTableAct->setShortcuts(QKeySequence::Print);
    printTableAct->setStatusTip(tr("Print table"));
    connect(printTableAct, &QAction::triggered, this,
            [&]{
                if(timer.isActive() || timer_2.isActive()){
                    on_stopButton_clicked();
                }else{
                    isStopButtonClicked=false;
                }
                this->printTable();
    });

    exportToPdfAct = new QAction(tr("&Export to PDF"), this);
    exportToPdfAct->setStatusTip(tr("Export table to PDF"));
    connect(exportToPdfAct, &QAction::triggered, this,
            [&]{
                if(timer.isActive() || timer_2.isActive()){
                    on_stopButton_clicked();
                }else{
                    isStopButtonClicked=false;
                }
                this->exportTable();
    });

    settingsAct = new QAction(tr("&Settings"), this);
    settingsAct->setStatusTip(tr("Open settings"));
    connect(settingsAct, &QAction::triggered, ui->tabWidget, [&]{ui->tabWidget->setCurrentIndex(1);});

    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    exitAct->setStatusTip(tr("Exit the application"));
    connect(exitAct, &QAction::triggered, this, &QWidget::close);
}

void MainWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(newTaskAct);
    fileMenu->addAction(showHistoryAct);
    fileMenu->addAction(printTableAct);
    fileMenu->addAction(exportToPdfAct);
    fileMenu->addAction(settingsAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);
}


void MainWindow::printTable()
{
    QPrinter printer(QPrinter::HighResolution);
    printer.setResolution(1200);

    QPainter painter;

    painter.begin(&printer);
    double xscale = printer.pageRect().width() / double(ui->tableView->width());
    double yscale = printer.pageRect().height() / double(ui->tableView->height());
    double scale = qMin(xscale, yscale);
    painter.translate(printer.paperRect().center());
    painter.scale(scale, scale);
    painter.translate(-width()/2, -height()/2);
    ui->tableView->render(&painter);
}

void MainWindow::exportTable() // Экспортирует в PDF, но в виде картинки
{
    QPrinter printer(QPrinter::HighResolution);
    printer.setResolution(1200);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName("table.pdf");

    QPainter painter;

    painter.begin(&printer);
    double xscale = printer.pageRect().width() / double(ui->tableView->width());
    double yscale = printer.pageRect().height() / double(ui->tableView->height());
    double scale = qMin(xscale, yscale);
    painter.translate(printer.paperRect().center());
    painter.scale(scale-2, scale-2);
    painter.translate(-width()/2, -height()/2);
    ui->tableView->render(&painter);
}

void MainWindow::openDatabase()
{
    QSqlDatabase currentTasks = QSqlDatabase::addDatabase("QSQLITE");
    currentTasks.setDatabaseName("currenttasks.sqlite");
    if(currentTasks.open()){
        qDebug() << "DB is connected.";
    }else{
        qDebug() << "DB is not connected.";
    }
}

void MainWindow::createDatabaseTable()
{
    QSqlQuery query;
    if(query.exec("CREATE TABLE IF NOT EXISTS TASKSLIST ("
                    "[Date]         TEXT, "
                    "[Task]         TEXT, "
                    "[Pomodoros]    INTEGER, "
                    "[Priority]     INTEGER);"))
    {
        qDebug() << "Table created";
    }else{
        qDebug() << "Table failed";
    }
}

void MainWindow::setUserFontProperties(QString appFont, QString tableFont)
{
//    ui->tabWidget->setStyleSheet(appFont);
    ui->tab->setStyleSheet(appFont);
//    ui->tab_2->setStyleSheet(appFont);
    ui->tableView->setStyleSheet(tableFont);
}

void MainWindow::on_applySettingsPushButton_clicked()
{
    writeSettings();
    this->setUserFontProperties(this->getAppFontProperties(), this->getTableFontProperties());
    historyForm->setUserFontProperties(this->getAppFontProperties(), this->getTableFontProperties());
    addTaskForm->setUserFontProperties(this->getAppFontProperties());
}

void MainWindow::on_resetPushButton_clicked()
{
    ui->appFontComboBox->setCurrentFont(QFont("MS Shell Dlg 2"));
    ui->appTextSizeSpinBox->setValue(12);
    ui->boldAppCheckBox->setChecked(0);
    ui->italicAppCheckBox->setChecked(0);

    ui->tableFontComboBox->setCurrentFont(QFont("Segoe UI"));
    ui->tableTextSizeSpinBox->setValue(12);
    ui->boldTableCheckBox->setChecked(0);
    ui->italicTableCheckBox->setChecked(0);
}

QString MainWindow::getAppFontProperties()
{
    QString appFont;
    appFont="font: ";
    ui->boldAppCheckBox->isChecked()?appFont+="bold ":appFont+="";
    ui->italicAppCheckBox->isChecked()?appFont+="italic ":appFont+="";
    appFont=appFont+QString::number(ui->appTextSizeSpinBox->value())+"px ";
    appFont+=ui->appFontComboBox->currentFont().family();
    return appFont;
}

QString MainWindow::getTableFontProperties()
{
    QString tableFont;
    tableFont="font: ";
    ui->boldTableCheckBox->isChecked()?tableFont+="bold ":tableFont+="";
    ui->italicTableCheckBox->isChecked()?tableFont+="italic ":tableFont+="";
    tableFont=tableFont+QString::number(ui->tableTextSizeSpinBox->value())+"px ";
    tableFont+=ui->tableFontComboBox->currentFont().family();
    return tableFont;
}
