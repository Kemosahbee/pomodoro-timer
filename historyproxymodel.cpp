#include "historyproxymodel.h"

HistoryProxyModel::HistoryProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{}

HistoryProxyModel::~HistoryProxyModel(){}

bool HistoryProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    QVariant leftData = sourceModel()->data(left);
    QVariant rightData = sourceModel()->data(right);

    if (left.column()==0) {
        QDateTime d1 = QDateTime::fromString(leftData.toString(), "dd.MM.yyyy h:mm");
        QDateTime d2 = QDateTime::fromString(rightData.toString(), "dd.MM.yyyy h:mm");
//        qDebug() << leftData.toString() << " | " << rightData.toString();
//        qDebug() << d1 << " | " << d2 << " | " << (d1<d2);
        return d1 > d2;
    }
    return false;
}

bool HistoryProxyModel::filterAcceptsRow(int sourceRow,
                                              const QModelIndex &sourceParent) const
{
    QModelIndex index0 = sourceModel()->index(sourceRow, 0, sourceParent);
    QModelIndex index1 = sourceModel()->index(sourceRow, 1, sourceParent);
    QModelIndex index2 = sourceModel()->index(sourceRow, 2, sourceParent);

    return (sourceModel()->data(index1).toString().contains(filterRegExp())
            && dateInRange(sourceModel()->data(index0).toString())
            && pomodorosInRange(sourceModel()->data(index2).toInt()));
}

bool HistoryProxyModel::dateInRange(const QString dateString) const
{
    QDateTime date = QDateTime::fromString(dateString, "dd.MM.yyyy hh:mm");
    return (!minDate.isValid() || date > minDate)
            && (!maxDate.isValid() || date < maxDate);
}

bool HistoryProxyModel::pomodorosInRange(const int p) const
{
    if(pFilter=="") return true;

    if (pFilter=="=="){
        return p==pomodoros;
    } else if (pFilter==">=") {
        return p>=pomodoros;
    } else if (pFilter=="<=") {
        return p<=pomodoros;
    } else if (pFilter==">") {
        return p>pomodoros;
    } else if (pFilter=="<") {
        return p<pomodoros;
    } else if (pFilter=="=") {
        return p==pomodoros;
    } else {
        return true;
    }
}

void HistoryProxyModel::setFilterMinimumDate(const QDateTime &date)
{
    minDate = date;
    invalidateFilter();
}

void HistoryProxyModel::setFilterMaximumDate(const QDateTime &date)
{
    maxDate = date;
    invalidateFilter();
}

void HistoryProxyModel::setPomodoros(const QString expression)
{
    QString s1=expression, s2=expression;
    s1.remove(QRegExp("[\\d]+"));
    s2.remove(QRegExp("[\\D]+"));
    pomodoros=s2.toInt();
    pFilter=s1;
    invalidateFilter();
}
