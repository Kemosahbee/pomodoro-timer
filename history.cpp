#include "history.h"
#include "ui_history.h"

History::History(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::History)
{
    ui->setupUi(this);

    createTable();

    model= new QSqlTableModel(this);
    model->setTable("HISTORY");
    model->select();

    proxyModel = new HistoryProxyModel;
    proxyModel->setSourceModel(model);
    proxyModel->sort(0); // Сортировка по дате

    // Настройка filterSyntaxComboBox
    ui->filterSyntaxComboBox->addItem(tr("Regular expression"), QRegExp::RegExp);
    ui->filterSyntaxComboBox->addItem(tr("Wildcard"), QRegExp::Wildcard);
    ui->filterSyntaxComboBox->addItem(tr("Fixed string"), QRegExp::FixedString);

//    // Настройка filterColumnComboBox
//    ui->filterColumnComboBox->addItem(tr("Date"));
//    ui->filterColumnComboBox->addItem(tr("Task"));
//    ui->filterColumnComboBox->addItem(tr("Pomodoros"));

    validator = new QRegExpValidator(QRegExp("[<=>][=]?[\\d]+")); // Валидатор для QLineEdit(дает заполнить только согласно маске)
    ui->pomodorosFilter->setValidator(validator);
    ui->pomodorosFilter->setPlaceholderText("== >= <= > <");

    connect(ui->filterPatternLineEdit, &QLineEdit::textChanged,
            this, &History::filterRegExpChanged);
    connect(ui->checkBox, &QCheckBox::stateChanged,
            this, &History::filterRegExpChanged);
    connect(ui->minDate, &QDateTimeEdit::dateChanged,
            this, &History::dateFilterChanged);
    connect(ui->maxDate, &QDateTimeEdit::dateChanged,
            this, &History::dateFilterChanged);
    connect(ui->pomodorosFilter, &QLineEdit::textChanged,
            this, &History::pomodorosFilterChanged);

//    ui->filterColumnComboBox->setCurrentIndex(1);

    ui->tableView->setModel(proxyModel);
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents); // Колонку Date растянуть по содержимому
    ui->tableView->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents); // Колонку Pomodooros растянуть по содержимому
    ui->tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch); // Колонку Task растянуть на все оставшееся место

    // Если строка не влазит в колонку, перенести на новую строку
    ui->tableView->setWordWrap(true);
    ui->tableView->setTextElideMode(Qt::ElideMiddle);
    ui->tableView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    //

    ui->tableView->hideColumn(3);                        // Скрыть колоку Приоритет
}

History::~History()
{
    delete ui;
}

void History::insertIntoTable(QVariantList &data)
{
    QSqlQuery query;
    query.prepare("INSERT INTO HISTORY "
                  "VALUES (:Date, :Task, :Pomodoros, :Priority )");
    query.bindValue(":Date",        data[0].toString());
    query.bindValue(":Task",        data[1].toString());
    query.bindValue(":Pomodoros",   data[2].toInt());
    query.bindValue(":Priority",    data[3].toInt());

    if(!query.exec()){
        qDebug() << "insertion failed";
    } else {
        qDebug() << "insertion success";
    }
}

void History::refresh()
{
    model->select();
}

void History::closeEvent(QCloseEvent *event)
{
    emit windowClosed();
    event->accept();
}

void History::filterRegExpChanged()
{
    QRegExp::PatternSyntax syntax =QRegExp::PatternSyntax(ui->filterSyntaxComboBox->itemData(ui->filterSyntaxComboBox->currentIndex()).toInt());
    Qt::CaseSensitivity caseSensitivity = ui->checkBox->isChecked() ? Qt::CaseSensitive:Qt::CaseInsensitive;
    QRegExp regExp(ui->filterPatternLineEdit->text(), caseSensitivity, syntax);

    proxyModel->setFilterRegExp(regExp);
}

//void History::filterColumnChanged()
//{
//    proxyModel->setFilterKeyColumn(ui->filterColumnComboBox->currentIndex());
//}

void History::dateFilterChanged()
{
    proxyModel->setFilterMinimumDate(ui->minDate->dateTime());
    proxyModel->setFilterMaximumDate(ui->maxDate->dateTime());
}

void History::pomodorosFilterChanged()
{
    proxyModel->setPomodoros(ui->pomodorosFilter->text());
}

void History::createTable()
{
    QSqlQuery query;
    if(query.exec("CREATE TABLE IF NOT EXISTS HISTORY ("
                    "[Date]         TEXT, "
                    "[Task]         TEXT, "
                    "[Pomodoros]    INTEGER, "
                    "[Priority]     INTEGER);"))
    {
        qDebug() << "Table created";
    }else{
        qDebug() << "Table failed";
    }
}

void History::setUserFontProperties(QString appFont, QString tableFont)
{
    this->setStyleSheet(appFont);
    ui->tableView->setStyleSheet(tableFont);
}
