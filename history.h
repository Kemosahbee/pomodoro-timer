#ifndef HISTORY_H
#define HISTORY_H

#include <QWidget>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlTableModel>
#include <QDateTime>
#include <QCloseEvent>
#include <QRegExpValidator>
#include "historyproxymodel.h"

namespace Ui {
class History;
}

class History : public QWidget
{
    Q_OBJECT

public:
    explicit History(QWidget *parent = nullptr);
    ~History() override;
    void insertIntoTable(QVariantList &data);
    void refresh();
    void setUserFontProperties(QString appFont, QString tableFont);

private slots:
    void filterRegExpChanged();
//    void filterColumnChanged();
    void dateFilterChanged();
    void pomodorosFilterChanged();


signals:
    void windowClosed();

protected:
    void closeEvent(QCloseEvent *event) override;

private:
    void createTable();

    Ui::History *ui;
    QSqlTableModel *model;
    HistoryProxyModel *proxyModel;
    QRegExpValidator *validator;
};



#endif // HISTORY_H
