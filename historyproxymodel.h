#ifndef HISTORYPROXYMODEL_H
#define HISTORYPROXYMODEL_H

#include <QSortFilterProxyModel>
#include <QDateTime>
#include <QtDebug>
#include <QVariant>

class HistoryProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_DISABLE_COPY(HistoryProxyModel)
public:
    explicit HistoryProxyModel(QObject *parent = nullptr);
    virtual ~HistoryProxyModel() override;

    void setFilterMinimumDate(const QDateTime &date);
    void setFilterMaximumDate(const QDateTime &date);
    void setPomodoros(const QString expression);

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;

private:
    bool dateInRange(const QString dateString) const;
    bool pomodorosInRange(const int p) const;

    QDateTime minDate;
    QDateTime maxDate;
    int pomodoros=0;
    QString pFilter="";
};

#endif // HISTORYPROXYMODEL_H
