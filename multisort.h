#ifndef MULTISORT_H
#define MULTISORT_H

#include <QSortFilterProxyModel>
#include <QList>
#include <QPair>
#include <QDateTime>
#include <QtDebug>
#include <QVariant>

class MultySortProxy : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_DISABLE_COPY(MultySortProxy)
public:
    explicit MultySortProxy(QObject *parent = nullptr);
    virtual ~MultySortProxy() override;
//    void addSortPriority(int col, int role = Qt::DisplayRole);
//    void removeSortPriority(int col, int role = Qt::DisplayRole);
//    void clearSortPriority();
//    void insertSortPriority(int level, int col, int role = Qt::DisplayRole);
//    QList<QPair<int, int>> m_sortPriority;

protected:
    bool lessThan(const QModelIndex & source_left, const QModelIndex & source_right) const override;
};

#endif // MULTISORT_H
