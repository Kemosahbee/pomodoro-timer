#ifndef MYPROGRESSBAR_H
#define MYPROGRESSBAR_H
#include <QProgressBar>
#include <QMouseEvent>

class MyProgressBar : public QProgressBar
{
public:
    MyProgressBar(QWidget *parent=nullptr);
    void mouseMoveEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
};

#endif // MYPROGRESSBAR_H
