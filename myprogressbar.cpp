#include "myprogressbar.h"

MyProgressBar::MyProgressBar(QWidget *parent) : QProgressBar(parent)
{

};

void MyProgressBar::mouseMoveEvent(QMouseEvent *event)
{
    this->setValue(this->maximum()*event->x()/width());
}


void MyProgressBar::mousePressEvent(QMouseEvent *event)
{
    this->setValue(this->maximum()*event->x()/width());
}
