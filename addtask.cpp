#include "addtask.h"
#include "ui_addtask.h"

AddTask::AddTask(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddTask)
{
    ui->setupUi(this);
    ui->textEdit->setFocus();
    ui->lineEdit->setPlaceholderText("1-5");
    connect(ui->lineEdit, &QLineEdit::textChanged, ui->lineEdit,
            [&]{
               if(ui->lineEdit->text().toInt()<1 || ui->lineEdit->text().toInt()>5){
                   ui->lineEdit->clear();
               }
    });
}

AddTask::~AddTask()
{
    delete ui;
}

void AddTask::on_okButton_clicked()
{
    if(!ui->textEdit->toPlainText().isEmpty()){
        emit saveTask();
    }else{
        emit cancelTask();
    }
    ui->textEdit->clear();
    ui->lineEdit->clear();
    this->close();
}

QVariantList AddTask::getText()
{
    QVariantList values;
    values.append(QDateTime::currentDateTime().toString(Qt::SystemLocaleShortDate));
    values.append(ui->textEdit->toPlainText());
    values.append(0);
    if(ui->lineEdit->text().isEmpty()){
        values.append(1);
    }else{
        values.append(ui->lineEdit->text());
    }
    return values;
}

void AddTask::on_cancelButton_clicked()
{
    emit cancelTask();
    ui->textEdit->clear();
    ui->lineEdit->clear();
    this->close();
}

bool AddTask::insertIntoTable(const QVariantList &data)
{
    QSqlQuery query;
    query.prepare("INSERT INTO TASKSLIST "
                  "VALUES (:Date, :Task, :Pomodoros, :Priority )");
    query.bindValue(":Date",        data[0].toString());
    query.bindValue(":Task",        data[1].toString());
    query.bindValue(":Pomodoros",   data[2].toInt());
    query.bindValue(":Priority",    data[3].toInt());

    if(!query.exec()){
        return false;
    } else {
        return true;
    }
}

void AddTask::closeEvent(QCloseEvent *event)
{
    emit windowClosed();
    event->accept();
}

void AddTask::setUserFontProperties(QString appFont)
{
    this->setStyleSheet(appFont);
}
