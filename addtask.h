#ifndef ADDTASK_H
#define ADDTASK_H
#include <QWidget>
#include <QDateTime>
#include <QSqlQuery>
#include <QCloseEvent>

QT_BEGIN_NAMESPACE
namespace Ui { class AddTask; }
QT_END_NAMESPACE

class AddTask : public QWidget
{
    Q_OBJECT

public:
    AddTask(QWidget *parent = nullptr);
    QVariantList getText();
    bool insertIntoTable(const QVariantList &data);
    void setUserFontProperties(QString appFont);
    ~AddTask() override;

private slots:
    void on_okButton_clicked();

    void on_cancelButton_clicked();

signals:
    void saveTask();
    void cancelTask();
    void windowClosed();

protected:
    void closeEvent(QCloseEvent *event) override;

private:
    Ui::AddTask *ui;
};
#endif // ADDTASK_H
