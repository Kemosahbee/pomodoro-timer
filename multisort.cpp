#include "multisort.h"

MultySortProxy::MultySortProxy(QObject *parent)
    : QSortFilterProxyModel(parent)
{}

MultySortProxy::~MultySortProxy(){}

//void MultySortProxy::addSortPriority(int col, int role)
//{
//    m_sortPriority.append(QPair<int, int>(col, role));
//}

//void MultySortProxy::removeSortPriority(int col, int role)
//{
//    m_sortPriority.removeAt(m_sortPriority.indexOf(QPair<int, int>(col, role)));
//}

//void MultySortProxy::clearSortPriority()
//{
//    m_sortPriority.clear();
//}

//void MultySortProxy::insertSortPriority(int level, int col, int role)
//{
//    m_sortPriority.insert(level, QPair<int, int>(col, role));
//}

//bool MultySortProxy::lessThan(const QModelIndex & source_left, const QModelIndex & source_right) const
//{
//    for (auto i = m_sortPriority.constBegin(); i != m_sortPriority.constEnd();++i){
//        const QVariant leftData = source_left.model()->index(source_left.row(), i->first, source_left.parent()).data(i->second);
//        const QVariant rightData = source_right.model()->index(source_right.row(), i->first, source_right.parent()).data(i->second);
//        if (i->first==0)
//        {
//            return (leftData.toDateTime()<rightData.toDateTime());
//        }else if (i->first==3){
//            return (leftData < rightData);
//        }
//    }
//    return false;
//}


// Все что закоментировано выше делало то же самое, что и функция ниже (Сортировка по нескольким колонкам, вроде работает)
bool MultySortProxy::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    QVariant leftData = sourceModel()->data(left);
    QVariant rightData = sourceModel()->data(right);

    if (left.column()==3) {
        return (leftData <= rightData);
    } else if (left.column()==0){
        QDateTime d1 = QDateTime::fromString(leftData.toString(), "dd.MM.yyyy h:mm");
        QDateTime d2 = QDateTime::fromString(rightData.toString(), "dd.MM.yyyy h:mm");
        return d1 < d2;
    }
    return false;
}
